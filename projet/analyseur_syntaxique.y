%{
#include<stdlib.h>
#include<stdio.h>
#include "syntabs.h" // pour syntaxe abstraite
#include "tabsymboles.h"
#include "util.h"
#define YYDEBUG 1
extern n_prog *n;   // pour syntaxe abstraite
extern FILE *yyin;    // declare dans compilo
extern int yylineno;  // declare dans analyseur lexical
int yylex();          // declare dans analyseur lexical
int yyerror(char *s); // declare ci-dessous
%}

%code requires
{
#include "syntabs.h"
}

%union {char *cval; int ival; n_exp * n_exp; n_l_exp * n_l_exp; n_instr * n_instr; n_l_instr * n_l_instr; n_dec * n_dec; n_l_dec * n_l_dec; n_var * n_var; n_prog * n_prog;}

%type <n_prog> programme

%type <n_var> var

%type <n_l_dec> declare declareFonc paramDefFonction paramDef liste_declare_prog liste_fonction_prog
%type <n_dec> defFonction var_declare

%type <n_l_instr> instruction
%type <n_instr> appelInstrFonc instrEcrire instrRetour instrAppel instrBloc instrTantQue instrPour option_sinon instrSi instrAffect

%type <n_l_exp> param paramAppel
%type <n_exp> lire expression e2 e3 e4 e5 e6 e7 appelFonction

//TOKEN LES TERMINAUX
//SYMBOLE
%token POINT_VIRGULE
%token VIRGULE
%token PARENTHESE_OUVRANTE
%token PARENTHESE_FERMANTE
%token CROCHET_OUVRANT
%token CROCHET_FERMANT
%token ACCOLADE_OUVRANTE
%token ACCOLADE_FERMANTE

//OPERATION
%token PLUS
%token MOINS
%token FOIS
%token DIVISE
%token EGAL
%token INFERIEUR
%token ET
%token OU
%token NON

//NOM DE VARIABLE
%token <cval> IDENTIF

//TYPE
%token ENTIER

//TYPE ENTRER
%token <ival> NOMBRE

//CONDITION
%token SI
%token ALORS
%token SINON

//BOUCLE
%token POUR
%token TANTQUE
%token FAIRE

//FONCTION
%token LIRE
%token ECRIRE

//RETOUR
%token RETOUR

%start programme
%%

//Axiome
//programme: ensemble { $$ = cree_n_prog();}
//| {$$ = NULL;}
//;

//Programme
//ensemble: declare ensemble {$$ = }
//| defFonction ensemble {$$ = }
//| {$$ = NULL;}
//;

programme: liste_declare_prog liste_fonction_prog { $$ = n = cree_n_prog ($1, $2); };

liste_declare_prog: declare liste_declare_prog {$$ = cree_concat_l_dec($2,$1);}
| {$$ = NULL;}
;

liste_fonction_prog : defFonction liste_fonction_prog {$$ = cree_n_l_dec($1,$2);}
| {$$ = NULL;}
;

//Instruction
instruction: instrAffect instruction { $$ = cree_n_l_instr($1,$2);}
| instrSi instruction { $$ = cree_n_l_instr($1,$2);}
| instrTantQue instruction { $$ = cree_n_l_instr($1,$2);}
| instrPour instruction { $$ = cree_n_l_instr($1,$2);}
| instrBloc instruction { $$ = cree_n_l_instr($1,$2);}
| instrAppel instruction { $$ = cree_n_l_instr($1,$2);}
| instrRetour instruction { $$ = cree_n_l_instr($1,$2);}
| instrEcrire instruction { $$ = cree_n_l_instr($1,$2);}
| {$$ = NULL;}
;

//Type d'instruction
instrAffect: var EGAL expression POINT_VIRGULE { $$ = cree_n_instr_affect($1,$3);};
instrSi: SI expression ALORS instrBloc option_sinon { $$ = cree_n_instr_si($2,$4,$5);};
option_sinon: SINON instrBloc { $$ = $2;};
| {$$ = NULL;}
;
instrTantQue: TANTQUE expression FAIRE instrBloc { $$ = cree_n_instr_tantque($2,$4);};
instrPour: POUR instrAffect  expression POINT_VIRGULE instrAffect FAIRE instrBloc { $$ = cree_n_instr_pour($2,$3,$5,$7);};
instrBloc: ACCOLADE_OUVRANTE instruction ACCOLADE_FERMANTE { $$ = cree_n_instr_bloc($2);};
instrAppel: appelInstrFonc { $$ = $1;};
instrRetour: RETOUR expression POINT_VIRGULE { $$ = cree_n_instr_retour($2);};
instrEcrire: ECRIRE PARENTHESE_OUVRANTE expression PARENTHESE_FERMANTE POINT_VIRGULE { $$ = cree_n_instr_ecrire($3);};

//Fonction
appelFonction: IDENTIF PARENTHESE_OUVRANTE paramAppel PARENTHESE_FERMANTE { $$ = cree_n_exp_appel(cree_n_appel($1,$3));};
appelInstrFonc: IDENTIF PARENTHESE_OUVRANTE paramAppel PARENTHESE_FERMANTE POINT_VIRGULE { $$ = cree_n_instr_appel(cree_n_appel($1,$3));};
defFonction: IDENTIF PARENTHESE_OUVRANTE paramDefFonction PARENTHESE_FERMANTE declareFonc instrBloc { $$ = cree_n_dec_fonc($1,$3,$5,$6);};
lire: LIRE PARENTHESE_OUVRANTE PARENTHESE_FERMANTE { $$ = cree_n_exp_lire();};

//Param Fonction
paramDefFonction: paramDef { $$ = $1; }
| {$$ = NULL;}
;
paramDef: type IDENTIF VIRGULE paramDef { $$ = cree_n_l_dec(cree_n_dec_var($2),$4);}
| type IDENTIF { $$ = cree_n_l_dec(cree_n_dec_var($2),NULL);}
;

paramAppel: param { $$ = $1; }
| {$$ = NULL;}
;
param: expression VIRGULE param { $$ = cree_n_l_exp($1,$3);}
| expression { $$ = cree_n_l_exp($1,NULL);}
;

//Declaration
declareFonc: declare { $$ = $1;}
| { $$ = NULL;}
;
declare: var_declare VIRGULE declare { $$ = cree_n_l_dec($1,$3);}
|  var_declare POINT_VIRGULE { $$ = cree_n_l_dec($1,NULL);}
;
var_declare : type IDENTIF { $$ = cree_n_dec_var($2);}
| type IDENTIF CROCHET_OUVRANT NOMBRE CROCHET_FERMANT {$$ = cree_n_dec_tab($2,$4);}
;

//Type
type: ENTIER;

//Variable SIMPLE
var: IDENTIF { $$ = cree_n_var_simple($1);}
| IDENTIF CROCHET_OUVRANT expression CROCHET_FERMANT {$$ = cree_n_var_indicee($1,$3);}
;

//Expression
expression: expression OU e2 { $$ = cree_n_exp_op(ou,$1,$3);}
| e2 { $$ = $1; }
;
e2: e2 ET e3 { $$ = cree_n_exp_op(et,$1,$3);}
| e3 { $$ = $1; }
;
e3: e3 EGAL e4 { $$ = cree_n_exp_op(egal,$1,$3);}
| e3 INFERIEUR e4 { $$ = cree_n_exp_op(inferieur,$1,$3);}
| e4 { $$ = $1; }
;
e4: e4 PLUS e5 { $$ = cree_n_exp_op(plus,$1,$3);}
| e4 MOINS e5 { $$ = cree_n_exp_op(moins,$1,$3);}
| e5 { $$ = $1; }
;
e5: e5 FOIS e6 { $$ = cree_n_exp_op(fois,$1,$3);}
| e5 DIVISE e6 { $$ = cree_n_exp_op(divise,$1,$3);}
| e6 { $$ = $1; }
;
e6: NON e6 { $$ = cree_n_exp_op(non,$2,NULL);}
| e7 { $$ = $1; }
;
e7: PARENTHESE_OUVRANTE expression PARENTHESE_FERMANTE { $$ = $2;}
| NOMBRE { $$ = cree_n_exp_entier($1);}
| appelFonction { $$ = $1;}
| lire { $$ = $1;}
| var { $$ = cree_n_exp_var($1);}
;


%%

int yyerror(char *s) {
  fprintf(stderr, "erreur de syntaxe ligne %d\n", yylineno);
  fprintf(stderr, "%s\n", s);
  fclose(yyin);
  exit(1);
}

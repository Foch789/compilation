# Compilation
Projet compilation
Laurent DOITEAU

Le projet à pour but de réalsier un compilateur.
Il vérifie si le code respecte la syntaxe attendue (par rapport au langage L).

Pour utiliser ce projet :
-Mettez vore terminal dans le dossier projet
-Utilisez  "make" sous terminal
Après cela vous avec un "compilo" qui est l'éxecutable (le compilateur).Il vous permettra de vérifier si votre langage L est juste.
Vous pouvez l'utiliser de différentes manières depuis le terminal.

commmande :
 -l (affiche les tokens de l'analyse lexicale)
 -s (affiche l'arbre de derivation)
 -a (affiche l'arbre abstrait)
 -t (affiche la table des symboles)
 -3 (affiche le code trois adresses)
 -n (affiche le code nasm (actif par defaut))
 -h (affiche ce message)

Pour utilisez l'éxecutable :
Sur le terminal --> ./compilo (commande) (chemin du fichier).l


Pour netoyer le dossier projet (sur le terminal) :
- "make clean"

#include <stdio.h>
#include "syntabs.h"
#include "tabsymboles.h"
#include "parcours_arbre_abstrait.h"
#include "util.h"
#include <string.h>
#include <stdlib.h>

void parcours_n_prog(n_prog *n);

void parcours_l_instr(n_l_instr *n);
void parcours_instr(n_instr *n);
void parcours_instr_si(n_instr *n);
void parcours_instr_tantque(n_instr *n);
void parcours_instr_affect(n_instr *n);
void parcours_instr_appel(n_instr *n);
void parcours_instr_retour(n_instr *n);
void parcours_instr_ecrire(n_instr *n);

void parcours_l_exp(n_l_exp *n);
operande* parcours_exp(n_exp *n);
operande* parcours_varExp(n_exp *n);
operande* parcours_opExp(n_exp *n);
operande* parcours_intExp(n_exp *n);
operande* parcours_lireExp(n_exp *n);
operande* parcours_appelExp(n_exp *n);

void parcours_l_dec(n_l_dec *n);
void parcours_dec(n_dec *n);
void parcours_foncDec(n_dec *n);
void parcours_varDec(n_dec *n);
void parcours_tabDec(n_dec *n);
operande* parcours_var(n_var *n);
operande* parcours_var_simple(n_var *n);
operande* parcours_var_indicee(n_var *n);
void parcours_appel(n_appel *n);

int affiche_tabsymb = 1;
int n_etiquette = 0;
/*-------------------------------------------------------------------------*/

void parcours_n_prog_for_nasm(n_prog *n)
{
  affiche_tabsymb = 0;
  parcours_n_prog(n);
}

void parcours_n_prog_affiche_code3a(n_prog *n)
{
  affiche_tabsymb = 0;
  parcours_n_prog(n);
  code3a_affiche_code();
}

void parcours_n_prog(n_prog *n)
{
  portee = P_VARIABLE_GLOBALE;
  adresseLocaleCourante = 0;
  adresseArgumentCourant = 0;
  adresseGlobaleCourante = 0;

  code3a_init();

  parcours_l_dec(n->variables);
  parcours_l_dec(n->fonctions);
}

/*-------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/

void parcours_l_instr(n_l_instr *n)
{
  if(n){
  parcours_instr(n->tete);
  parcours_l_instr(n->queue);
  }
}

/*-------------------------------------------------------------------------*/

void parcours_instr(n_instr *n)
{
  if(n){
    if(n->type == blocInst) parcours_l_instr(n->u.liste);
    else if(n->type == affecteInst) parcours_instr_affect(n);
    else if(n->type == siInst) parcours_instr_si(n);
    else if(n->type == tantqueInst) parcours_instr_tantque(n);
    else if(n->type == appelInst) parcours_instr_appel(n);
    else if(n->type == retourInst) parcours_instr_retour(n);
    else if(n->type == ecrireInst) parcours_instr_ecrire(n);
  }
}

/*-------------------------------------------------------------------------*/

void parcours_instr_si(n_instr *n)
{

  int a = 0;
  a = new_etiquette_perso();
  char * etiq_debut =  malloc (sizeof (a)+1);
  sprintf(etiq_debut,"e%d",a);

  int b = 0;
  b = new_etiquette_perso();
  char * etiq_sinon =  malloc (sizeof (b)+1);
  sprintf(etiq_sinon,"e%d",b);

  if(!n->u.si_.sinon){
    etiq_debut = etiq_sinon;
  }

  operande * result = parcours_exp(n->u.si_.test);
  code3a_ajoute_instruction(jump_if_equal,result,code3a_new_constante(0),code3a_new_etiquette(etiq_debut),NULL);

  parcours_instr(n->u.si_.alors);
  if(n->u.si_.sinon){
    code3a_ajoute_instruction(jump,code3a_new_etiquette(etiq_sinon),NULL,NULL,NULL);
    code3a_ajoute_etiquette(etiq_debut);
    etiq_debut = etiq_sinon;
    parcours_instr(n->u.si_.sinon);
  }

  code3a_ajoute_etiquette(etiq_debut);


}

/*-------------------------------------------------------------------------*/

void parcours_instr_tantque(n_instr *n)
{
  int a = 0,b = 0;
  a = new_etiquette_perso();
  char * etiq_debut =  malloc (sizeof (a)+1);
  sprintf(etiq_debut,"e%d",a);

  b = new_etiquette_perso();
  char * etiq_fin =  malloc (sizeof (b)+1);
  sprintf(etiq_fin,"e%d",b);

  code3a_ajoute_etiquette(etiq_debut);
  operande * var = parcours_exp(n->u.tantque_.test);

  code3a_ajoute_instruction(jump_if_equal,var,code3a_new_constante(0),code3a_new_etiquette(etiq_fin),NULL);
  parcours_instr(n->u.tantque_.faire);
  code3a_ajoute_instruction(jump,code3a_new_etiquette(etiq_debut),NULL,NULL,NULL);
  code3a_ajoute_etiquette(etiq_fin);

}

/*-------------------------------------------------------------------------*/

void parcours_instr_affect(n_instr *n)
{

  operande *t0 =  parcours_exp(n->u.affecte_.exp);
  operande *var = parcours_var(n->u.affecte_.var);

  code3a_ajoute_instruction(assign,t0,NULL,var,NULL);
}

/*-------------------------------------------------------------------------*/

void parcours_instr_appel(n_instr *n)
{
  parcours_appel(n->u.appel);

  char *  nom =  malloc (sizeof (n->u.appel->fonction)+1);
  nom[0] = 'f';
  strcat(nom,n->u.appel->fonction);

  code3a_ajoute_instruction(func_call,code3a_new_etiquette(nom),NULL,NULL,NULL);
  free(nom);
}
/*-------------------------------------------------------------------------*/

void parcours_appel(n_appel *n)
{
  int nbLigne = 0;
  code3a_ajoute_instruction(alloc,code3a_new_constante(1),NULL,NULL,"alloue de la place pour la valeur de retour");
  if((nbLigne = rechercheExecutable(n->fonction)) != -1)
  {
    if(tabsymboles.tab[nbLigne].complement == compte_n_l_exp(n->args))
    {
      parcours_l_exp(n->args);
    }
    else
      erreur((char *)"appel avec trop de param");
  }
  else
    erreur((char *)"La fonction ne peut être utilisé");
}

/*-------------------------------------------------------------------------*/

void parcours_instr_retour(n_instr *n)
{
  operande * result = parcours_exp(n->u.retour_.expression);
  code3a_ajoute_instruction(func_val_ret,result,NULL,NULL,"sauvegarder la valeur de retour");
  code3a_ajoute_instruction(func_end,NULL,NULL,NULL,"terminer l'exécution de la fonction");
}

/*-------------------------------------------------------------------------*/

void parcours_instr_ecrire(n_instr *n)
{
  operande * result = parcours_exp(n->u.ecrire_.expression);
  code3a_ajoute_instruction(sys_write,result,NULL,NULL,NULL);
}

/*-------------------------------------------------------------------------*/

void parcours_l_exp(n_l_exp *n)
{
  operande* result = NULL;
  if(n){
    result = parcours_exp(n->tete);
    code3a_ajoute_instruction(func_param,result,NULL,NULL,NULL);
    parcours_l_exp(n->queue);
  }
}

/*-------------------------------------------------------------------------*/

operande* parcours_exp(n_exp *n)
{
  operande* result = NULL;

  if(n->type == varExp) result = parcours_varExp(n);
  else if(n->type == opExp) result = parcours_opExp(n);
  else if(n->type == intExp) result = parcours_intExp(n);
  else if(n->type == appelExp) result = parcours_appelExp(n);
  else if(n->type == lireExp) result = parcours_lireExp(n);

  return result;
}

/*-------------------------------------------------------------------------*/

operande* parcours_varExp(n_exp *n)
{
  operande* result = NULL;

  result = parcours_var(n->u.var);

  return result;
}

/*-------------------------------------------------------------------------*/
operande* parcours_opExp(n_exp *n)
{
  operande* result = NULL;
  operande *t0 = NULL;
  operande *t1 = NULL;

  operande* tempOU = NULL;
  int l = 0;
  char * etiq_debut_ou = NULL;

  operande* tempET = NULL;
  int etl = 0;
  char * etiq_debut_ET = NULL;



  if( n->u.opExp_.op1 != NULL ) {
    t0 =  parcours_exp(n->u.opExp_.op1);
    if(n->u.opExp_.op == ou)
    {
      l = new_etiquette_perso();
      etiq_debut_ou =  malloc (sizeof (l)+1);
      sprintf(etiq_debut_ou,"e%d",l);

      tempOU = code3a_new_temporaire();
      code3a_ajoute_instruction(assign,code3a_new_constante(-1),NULL,tempOU,"Debut du ou");
      code3a_ajoute_instruction(jump_if_equal,t0,code3a_new_constante(-1),code3a_new_etiquette(etiq_debut_ou),NULL);
    }
    else if(n->u.opExp_.op == et)
    {
      etl = new_etiquette_perso();
      etiq_debut_ET =  malloc (sizeof (etl)+1);
      sprintf(etiq_debut_ET,"e%d",etl);

      tempET = code3a_new_temporaire();
      code3a_ajoute_instruction(assign,code3a_new_constante(0),NULL,tempET,"Debut du et");
      code3a_ajoute_instruction(jump_if_equal,t0,code3a_new_constante(0),code3a_new_etiquette(etiq_debut_ET),NULL);

    }
    else if (n->u.opExp_.op == non)
    {

      int nonl1 = 0;
      char * etiq_debut_non1 = NULL;

      int nonl2 = 0;
      char * etiq_debut_non2 = NULL;

      nonl1 = new_etiquette_perso();
      etiq_debut_non1 =  malloc (sizeof (nonl1)+1);
      sprintf(etiq_debut_non1,"e%d",nonl1);

      nonl2 = new_etiquette_perso();
      etiq_debut_non2 =  malloc (sizeof (nonl2)+1);
      sprintf(etiq_debut_non2,"e%d",nonl2);

      code3a_ajoute_instruction(jump_if_equal,t0,code3a_new_constante(0),code3a_new_etiquette(etiq_debut_non1),"Debut non");
      code3a_ajoute_instruction(assign,code3a_new_constante(0),NULL,t0,NULL);
      code3a_ajoute_instruction(jump,code3a_new_etiquette(etiq_debut_non2),NULL,NULL,NULL);
      code3a_ajoute_etiquette(etiq_debut_non1);
      code3a_ajoute_instruction(assign,code3a_new_constante(-1),NULL,t0,NULL);
      code3a_ajoute_etiquette(etiq_debut_non2);

      result = t0;

    }
  }
  if( n->u.opExp_.op2 != NULL ) {
    t1 = parcours_exp(n->u.opExp_.op2);
    if(n->u.opExp_.op == ou)
    {
      code3a_ajoute_instruction(jump_if_equal,t1,code3a_new_constante(-1),code3a_new_etiquette(etiq_debut_ou),NULL);
      code3a_ajoute_instruction(assign,code3a_new_constante(0),NULL,tempOU,NULL);

    }
    else if(n->u.opExp_.op == et)
    {
      code3a_ajoute_instruction(jump_if_equal,t1,code3a_new_constante(0),code3a_new_etiquette(etiq_debut_ET),NULL);
      code3a_ajoute_instruction(assign,code3a_new_constante(1),NULL,tempET,NULL);
    }
  }


  if(n->u.opExp_.op == plus)
  {
    code3a_ajoute_instruction(arith_add,t0,t1,result = code3a_new_temporaire(),NULL);
  }
  else if(n->u.opExp_.op == moins)
  {
    code3a_ajoute_instruction(arith_sub,t0,t1,result = code3a_new_temporaire(),NULL);
  }
  else if(n->u.opExp_.op == fois)
  {
    code3a_ajoute_instruction(arith_mult,t0,t1,result = code3a_new_temporaire(),NULL);
  }
  else if(n->u.opExp_.op == divise)
  {
    code3a_ajoute_instruction(arith_div,t0,t1,result = code3a_new_temporaire(),NULL);
  }
  else if(n->u.opExp_.op == inferieur)
  {

    int a = 0;
    a = new_etiquette_perso();
    char * etiq_debut =  malloc (sizeof (a)+1);
    sprintf(etiq_debut,"e%d",a);

    operande* temp1 = code3a_new_temporaire();

    code3a_ajoute_instruction(assign,code3a_new_constante(-1),NULL,temp1,NULL);
    code3a_ajoute_instruction(jump_if_less,t0,t1,code3a_new_etiquette(etiq_debut),NULL);
    code3a_ajoute_instruction(assign,code3a_new_constante(0),NULL,temp1,NULL);

    result = temp1;

    code3a_ajoute_etiquette(etiq_debut);

  }
  else if(n->u.opExp_.op == egal)
  {
    int a = 0;
    a = new_etiquette_perso();
    char * etiq_debut =  malloc (sizeof (a)+1);
    sprintf(etiq_debut,"e%d",a);

    operande* temp1 = code3a_new_temporaire();

    code3a_ajoute_instruction(assign,code3a_new_constante(-1),NULL,temp1,NULL);
    code3a_ajoute_instruction(jump_if_equal,t0,t1,code3a_new_etiquette(etiq_debut),NULL);
    code3a_ajoute_instruction(assign,code3a_new_constante(0),NULL,temp1,NULL);

    result = temp1;

    code3a_ajoute_etiquette(etiq_debut);
  }
  else if(n->u.opExp_.op == ou)
  {

    code3a_ajoute_etiquette(etiq_debut_ou);
    result = tempOU;

  }
  else if(n->u.opExp_.op == et)
  {

    code3a_ajoute_etiquette(etiq_debut_ET);
    result = tempET;

  }



  return result;
}

/*-------------------------------------------------------------------------*/

operande* parcours_intExp(n_exp *n)
{
  operande* result = NULL;

  result = code3a_new_constante(n->u.entier);

  return result;
}

/*-------------------------------------------------------------------------*/
operande* parcours_lireExp(n_exp *n)
{
  operande* result = code3a_new_temporaire();

  code3a_ajoute_instruction(sys_read,NULL,NULL,result,NULL);

  return result;
}

/*-------------------------------------------------------------------------*/

operande* parcours_appelExp(n_exp *n)
{
  operande* result = code3a_new_temporaire();
  parcours_appel(n->u.appel);

  char *  nom =  malloc (sizeof (n->u.appel->fonction)+1);
  nom[0] = 'f';
  strcat(nom,n->u.appel->fonction);
  code3a_ajoute_instruction(func_call,code3a_new_etiquette(nom),NULL,result,NULL);

  return result;

}

/*-------------------------------------------------------------------------*/

void parcours_l_dec(n_l_dec *n)
{
  if( n ){
    parcours_dec(n->tete);
    parcours_l_dec(n->queue);
  }
}

/*-------------------------------------------------------------------------*/

void parcours_dec(n_dec *n)
{
  if(n){
    if(n->type == foncDec) {
      parcours_foncDec(n);
    }
    else if(n->type == varDec) {
      parcours_varDec(n);
    }
    else if(n->type == tabDec) {
      parcours_tabDec(n);
    }
  }
}

/*-------------------------------------------------------------------------*/

void parcours_foncDec(n_dec *n)
{

  if(rechercheDeclarative(n->nom) == -1)
  {
    ajouteIdentificateur(n->nom,portee,T_FONCTION,0,compte_n_l_dec(n->u.foncDec_.param));

    char *  nom =  malloc (sizeof (n->nom)+1);
    nom[0] = 'f';
    strcat(nom,n->nom);
    code3a_ajoute_etiquette(nom);
    code3a_ajoute_instruction(func_begin,NULL,NULL,NULL,NULL);

    entreeFonction();

    parcours_l_dec(n->u.foncDec_.param);

    portee = P_VARIABLE_LOCALE;

    parcours_l_dec(n->u.foncDec_.variables);
    parcours_instr(n->u.foncDec_.corps);
    sortieFonction(affiche_tabsymb);
    code3a_ajoute_instruction(func_end,NULL,NULL,NULL,NULL);

  }
  else
  {
    erreur((char *)"La fonction est  deja existante");
  }

}

/*-------------------------------------------------------------------------*/

void parcours_varDec(n_dec *n)
{
  if(portee == P_VARIABLE_GLOBALE)
  {
    if(rechercheDeclarative(n->nom) == -1)
    {
      ajouteIdentificateur(n->nom,portee,T_ENTIER,adresseGlobaleCourante,T_ENTIER);
      code3a_ajoute_instruction(alloc,code3a_new_constante(1),code3a_new_var(n->nom,portee,adresseGlobaleCourante),NULL,(char *)n->nom);
      adresseGlobaleCourante+=4;
    }
    else
    {
      erreur((char *)"Variable deja existante");
    }
  }else if (portee == P_VARIABLE_LOCALE)
  {
    if(rechercheDeclarative(n->nom) == -1)
    {
      ajouteIdentificateur(n->nom,portee,T_ENTIER,adresseLocaleCourante,T_ENTIER);
      code3a_ajoute_instruction(alloc,code3a_new_constante(1),code3a_new_var(n->nom,portee,adresseLocaleCourante),NULL,(char *)n->nom);
      adresseLocaleCourante+=4;
    }
    else
    {
      erreur((char *)"Variable locale deja existante");
    }
  }
  else
  {
    if(rechercheDeclarative(n->nom) == -1)
    {
      ajouteIdentificateur(n->nom,portee,T_ENTIER,adresseArgumentCourant,T_ENTIER);
      adresseArgumentCourant+=4;
    }
    else
    {
      erreur((char *)"Variable locale deja existante");
    }
  }
}

/*-------------------------------------------------------------------------*/

void parcours_tabDec(n_dec *n)
{

  if(portee == P_VARIABLE_GLOBALE)
  {
    if(rechercheDeclarative(n->nom) == -1)
    {
      ajouteIdentificateur(n->nom,portee,T_TABLEAU_ENTIER,adresseGlobaleCourante,n->u.tabDec_.taille);
      code3a_ajoute_instruction(alloc,code3a_new_constante(n->u.tabDec_.taille),code3a_new_var(n->nom,portee,adresseGlobaleCourante),NULL,(char *)n->nom);
      adresseGlobaleCourante+=(4*n->u.tabDec_.taille);
    }
    else
    {
      erreur((char *)"Variable deja existante");
    }
  }else if (portee == P_VARIABLE_LOCALE)
  {
    if(rechercheDeclarative(n->nom) == -1)
    {
      ajouteIdentificateur(n->nom,portee,T_TABLEAU_ENTIER,adresseLocaleCourante,n->u.tabDec_.taille);
      adresseLocaleCourante+=(4*n->u.tabDec_.taille);
    }
    else
    {
      erreur((char *)"Variable locale deja existante");
    }
  }
  else
  {
    if(rechercheDeclarative(n->nom) == -1)
    {
      ajouteIdentificateur(n->nom,portee,T_TABLEAU_ENTIER,adresseArgumentCourant,n->u.tabDec_.taille);
      adresseArgumentCourant+=(4*n->u.tabDec_.taille);
    }
    else
    {
      erreur((char *)"Variable locale deja existante");
    }
  }

}

/*-------------------------------------------------------------------------*/

operande* parcours_var(n_var *n)
{
  operande* result = NULL;

  if(n->type == simple) {
    result = parcours_var_simple(n);
  }
  else if(n->type == indicee) {
    result = parcours_var_indicee(n);
  }

  return result;
}

/*-------------------------------------------------------------------------*/
operande* parcours_var_simple(n_var *n)
{
  int nbLigne = rechercheExecutable(n->nom);

  operande* result = code3a_new_var(n->nom,tabsymboles.tab[nbLigne].portee,tabsymboles.tab[nbLigne].adresse);

  return result;
}

/*-------------------------------------------------------------------------*/
operande* parcours_var_indicee(n_var *n)
{
  operande* indice = parcours_exp( n->u.indicee_.indice );
  operande* result = NULL;

  int nbLigne = rechercheExecutable(n->nom);

  if(indice->oper_type == O_VARIABLE)
  {
    operande* temp = code3a_new_temporaire();
    code3a_ajoute_instruction(assign,indice,NULL,temp,NULL);
    result = code3a_new_var_indicee(n->nom,tabsymboles.tab[nbLigne].portee,tabsymboles.tab[nbLigne].adresse,temp);
  }
  else
  {
    result = code3a_new_var_indicee(n->nom,tabsymboles.tab[nbLigne].portee,tabsymboles.tab[nbLigne].adresse,indice);
  }

  return result;
}
/*-------------------------------------------------------------------------*/


int compte_n_l_dec(n_l_dec *n)
{
  int i =0;
  if(n != NULL)
  {
    if(n->tete != NULL)
    {
      i = 1;
      n_l_dec *l = n;
      while(l->queue != NULL)
      {
        i++;
        l = l->queue;
      }
    }
  }
  return i;
}

int compte_n_l_exp(n_l_exp *n)
{
  int i =0;
  if(n != NULL)
  {
    if(n->tete != NULL)
    {
      i = 1;
      n_l_exp *l = n;
      while(l->queue != NULL)
      {
        i++;
        l = l->queue;
      }
    }
  }
  return i;
}

int new_etiquette_perso()
{
  return n_etiquette++;
}

#ifndef __PARCOURS_ARBRE_ABSTRAIT__
#define __PARCOURS_ARBRE_ABSTRAIT__

#include "syntabs.h"
#include "code3a.h"

void parcours_n_prog(n_prog *n);
void parcours_n_prog_affiche_code3a(n_prog *n);
void parcours_n_prog_for_nasm(n_prog *n);
int compte_n_l_dec(n_l_dec *n);
int compte_n_l_exp(n_l_exp *n);
int new_etiquette_perso();

#endif
